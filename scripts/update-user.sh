#!/bin/bash

if test -z "$HOST"; then
  echo Please define host in environment variable HOST.
  exit 1
fi

defaultId=1
defaultEmail=mire@iki.fi
defaultFirstName=Mikko
defaultLastName=Reinikainen
defaultPassword=password
defaultNickname=MikkoR

echo "Please enter information for user to update:"
read -p "id[$defaultId]: " id
read -p "email[$defaultEmail]: " email
read -p "firstName[$defaultFirstName]: " firstName
read -p "lastName[$defaultLastName]: " lastName
read -p "password[$defaultPassword]: " password
read -p "nickname[$defaultNickname]: " nickname

if test -z "$id"; then
  id=$defaultId
fi
if test -z "$email"; then
  email=$defaultEmail
fi
if test -z "$firstName"; then
  firstName=$defaultFirstName
fi
if test -z "$lastName"; then
  lastName=$defaultLastName
fi
if test -z "$password"; then
  password=$defaultPassword
fi
if test -z "$nickname"; then
  nickname=$defaultNickname
fi

data="email=${email}&firstName=${firstName}&lastName=${lastName}&password=${password}&nickname=${nickname}"
url="${HOST}users/${id}"

command="curl -X PUT -u $email --data $data $url"
echo $command
curl -X PUT -u $email --data $data $url
echo
