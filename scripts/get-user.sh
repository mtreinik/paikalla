#!/bin/bash

if test -z "$HOST"; then
  echo Please define host in environment variable HOST.
  exit 1
fi

defaultId=1
defaultEmail=mire@iki.fi

echo "Please enter information for user to get:"
read -p "id[$defaultId]: " id
read -p "email[$defaultEmail]: " email

if test -z "$id"; then
  id=$defaultId
fi
if test -z "$email"; then
  email=$defaultEmail
fi

url="${HOST}users/${id}"

command="curl -u ${email} ${url}"
echo $command
curl -u ${email} ${url}
echo
