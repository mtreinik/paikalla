#!/bin/bash

if test -z "$HOST"; then
  echo Please define host in environment variable HOST.
  exit 1
fi
url="${HOST}users"

defaultOrganizationId=1
defaultEmail=mire@iki.fi
defaultFirstName=Mikko
defaultLastName=Reinikainen
defaultPassword=password
defaultNickname=MikkoR
defaultRole=user

echo "Please enter information for new user:"
read -p "organizationId[$defaultOrganizationId]: " organizationId
read -p "email[$defaultEmail]: " email
read -p "firstName[$defaultFirstName]: " firstName
read -p "lastName[$defaultLastName]: " lastName
read -p "password[$defaultPassword]: " password
read -p "nickname[$defaultNickname]: " nickname
read -p "role[$defaultRole]: " role

if test -z "$organizationId"; then
  organizationId=$defaultOrganizationId
fi
if test -z "$email"; then
  email=$defaultEmail
fi
if test -z "$firstName"; then
  firstName=$defaultFirstName
fi
if test -z "$lastName"; then
  lastName=$defaultLastName
fi
if test -z "$password"; then
  password=$defaultPassword
fi
if test -z "$nickname"; then
  nickname=$defaultNickname
fi
if test -z "$role"; then
  role=$defaultRole
fi

data="organizationId=${organizationId}&email=${email}&firstName=${firstName}&lastName=${lastName}&password=${password}&nickname=${nickname}&role=${role}"
command="curl --data \"${data}\" ${url}"
echo $command
curl --data "${data}" ${url}
echo
