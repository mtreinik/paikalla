#!/bin/bash

if test -z "$HOST"; then
  echo Please define host in environment variable HOST.
  exit 1
fi

defaultEmail=mire@iki.fi

echo "Please enter email address:"
read -p "email[$defaultEmail]: " email

if test -z "$email"; then
  email=$defaultEmail
fi

url="${HOST}users"

echo Getting users

command="curl -u ${email} ${url}"
echo $command
curl -u ${email} ${url}
echo
