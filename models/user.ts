export type Role = "manager" | "admin" | "user" | "deleted";

export interface User {
  id: number;
  organizationId: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  nickname: string;
  role: Role;
}
