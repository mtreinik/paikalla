import { Request, Response, RequestHandler } from 'express';
import { IDatabase } from 'pg-promise';
import { errorHandlerForResponse } from './controllerUtils';

export const getTimestamp = (db: IDatabase<{}>): RequestHandler => (_req: Request, res: Response): void => {
  db.one('select now()')
    .then((data: string[]): void => {
      res.send(data);
    })
    .catch(errorHandlerForResponse(res));
}
