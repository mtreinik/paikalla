import { Request, Response  } from 'express';
import { IDatabase } from 'pg-promise';
import bcrypt from 'bcryptjs';
import { BasicVerifyFunction } from 'passport-http';
import { User, Role } from '../models/user';

type Db = IDatabase<{}>;

interface VerifiedUser {
  id: number;
  role: Role;
};

export type VerifiedUserOrBoolean = VerifiedUser | boolean;
export type VerifyError = Error | undefined;

export const getPasswordHash = (password: string): string => {
  const salt = bcrypt.genSaltSync(12);
  return bcrypt.hashSync(password, salt);
};

export const checkUserIsAuthenticated =
  (userId: string, request: Request, response: Response): boolean => {
    if (!userId || !request || !request.user ||
        ('' + userId) !== ('' + request.user.id)) {
      response.status(401).send('Unauthorized.');
      return false;
    }
    return true;
  }

/*
TODO this is not yet needed
export const checkUserHasOneOfRoles =
  (roles: string, request: Request, response: Response): boolean => {
    if (!roles.includes(request.user.role)) {
      response.status(401).send('Unauthorized');
      return false;
    }
    return true;
  }
*/

export const verifyUser =
  (db: Db): BasicVerifyFunction =>
    (email: string, password: string, done: (error: VerifyError, user?: VerifiedUserOrBoolean) => void): void => {
      db.oneOrNone<User>('select id, password, role from users where email = $1', email)
        .then((data: User | null): void => {
          if (data === null) {
            return done(undefined, false);
          }
          if (bcrypt.compareSync(password, data.password)) {
            return done(undefined, {
              id: data.id,
              role: data.role
            });
          } else {
            return done(undefined, false);
          }
        })
        .catch((error: Error): void => {
          return done(error);
        });
    }
