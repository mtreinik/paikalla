import { Response } from 'express';

export const errorHandlerForResponse =
  (res: Response): ((e: Error) => void) => (error: Error): void => {
    console.error('error handler: setting status 500 and sending error', error);
    res.status(500).send(error);
  };
