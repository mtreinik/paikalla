import { Request, Response, RequestHandler } from 'express';
import { IDatabase } from 'pg-promise';
import { errorHandlerForResponse } from './controllerUtils';
import { User } from '../models/user';
import { checkUserIsAuthenticated, getPasswordHash } from './accessControllers';

type Db = IDatabase<{}>;

export const getUsers =
  (db: Db): RequestHandler =>
    (_req: Request, res: Response): void => {
      db.any<User>('select id, organizationId, email, ' +
        'first_name as firstName, last_name as lastName, nickname, role ' +
        'from users order by id')
        .then((data: User[]): void => {
          res.send(data);
        })
        .catch(errorHandlerForResponse(res));
    }

export const getUserById =
  (db: Db): RequestHandler =>
    (req: Request, res: Response): void => {
      const userId = req.params.id;
      if (checkUserIsAuthenticated(userId, req, res)) {
        db.oneOrNone<User>('select id, organizationId, email, ' +
          'first_name as firstName, last_name as lastName, nickname, role ' +
          'from users where id = $1', userId)
          .then((data: User | null): void => {
            if (data) {
              res.send(data);
            } else {
              res.status(404).send(`No user found with id ${userId}.`);
            }
          })
          .catch(errorHandlerForResponse(res));
      }
    }

export const addUser =
  (db: Db): RequestHandler =>
    (req: Request, res: Response): void => {
      const { organizationId, email, firstName, lastName, nickname, role } = req.body;
      const hashedPassword = getPasswordHash(req.body.password);
      db.oneOrNone<User>('insert into users ' +
        '(organizationId, ' +
        'email, ' +
        'password, ' +
        'first_name, ' +
        'last_name, ' +
        'nickname, ' +
        'role) ' +
        'values ($1, $2, $3, $4, $5, $6, $7) returning id',
      [organizationId, email, hashedPassword, firstName, lastName, nickname, role])
        .then((data: User | null): void => {
          if (data) {
            res.status(201).send(data);
          } else {
            res.status(500).send('Unable to save user.');
          }
        })
        .catch(errorHandlerForResponse(res));
    }

export const updateUser =
  (db: Db): RequestHandler =>
    (req: Request, res: Response): void => {
      const userId = req.params.id;
      if (checkUserIsAuthenticated(userId, req, res)) {
        const { email, firstName, lastName, nickname } = req.body;
        const hashedPassword = getPasswordHash(req.body.password);
        db.oneOrNone<User>('update users set ' +
          'email = $1, ' +
          'password = $2, ' +
          'first_name = $3, ' +
          'last_name = $4, ' +
          'nickname = $5 ' +
          'where id = $6 returning id',
        [email, hashedPassword, firstName, lastName, nickname, userId])
          .then((data: User | null): void => {
            if (data) {
              res.send(data);
            } else {
              res.status(500).send('Unable to update user.');
            }
          })
          .catch(errorHandlerForResponse(res));
      }
    }

export const deleteUser =
  (db: Db): RequestHandler =>
    (req: Request, res: Response): void => {
      const userId = req.params.id;
      if (checkUserIsAuthenticated(userId, req, res)) {
        db.oneOrNone<User>('delete from users where id = $1 returning id', userId)
          .then((data: User | null): void => {
            if (data) {
              res.send(data);
            } else {
              res.status(500).send('Unable to delete user.');
            }
          })
          .catch(errorHandlerForResponse(res));
      }
    }
