# Paikalla

Web service for tracking event attendance of a group of people.

Work in progress.

## Building and deploying

```
npm install
npm run build
```

`Dockerfile` contains instructions how to build an image of the app.

This project is deploying to an Google Cloud Platform Kubernetes Engine.

## The API

Server implements the following RESTful API:

| URL | method | purpose |
| --- | --- | --- |
| /users | GET | list users |
| /users/\<id\> | GET | get user with id <id> |
| /users/ | POST | add user |
| /users/\<id\> | PUT | update user |
| /users/\<id\> | DELETE | delete user |

Client must authenticate with the API by using HTTP Basic Authentication, i.e. username and password.

### User model

User data model contains the following fields:

```
organizationId: number
email: string
password: string
firstName: string
lastName: string
nickname: string
role: string (possible values: 'admin', 'manager', 'user')
```

## Technology

This server implements a RESTful API built on the [express.js](https://expressjs.com/) web framework for [Node.js](https://nodejs.org/).

The implementation is is done in [TypeScript](https://www.typescriptlang.org/) and the code is linted with [ESLint](https://eslint.org/).

The API uses HTTP Basic authentication with the help of [passport.js](http://www.passportjs.org/) middleware and [Passport-HTTP](https://github.com/jaredhanson/passport-http) authentication strategy. Password hashes are calculated by using the [bcryptjs](https://github.com/dcodeIO/bcrypt.js#readme) library.

The API connects to a Postgresql back-end by using the [pg-promise](https://github.com/vitaly-t/pg-promise) interface.

[morgan](https://github.com/expressjs/morgan#readme) logging middleware is used for logging.

Unit testing is done with [Mocha](https://mochajs.org/) and [chai](https://www.chaijs.com/) as assertion library and [substitute.js](https://www.npmjs.com/package/@fluffy-spoon/substitute) for mocking. The Mocha tests are executed with [ts-node](https://github.com/TypeStrong/ts-node).

Making HTTP requests to the API are tested with [supertest](https://github.com/visionmedia/supertest#readme).

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).
