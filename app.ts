import express from 'express';
import compression from 'compression';
import logger from 'morgan';
import pgPromise from 'pg-promise';
import {IMain, IDatabase, IOptions} from 'pg-promise';
import passport from 'passport';
import { BasicStrategy }  from 'passport-http';

import { getTimestamp } from './controllers/miscControllers';
import { getUsers, getUserById, addUser, updateUser, deleteUser } from './controllers/userControllers'
import { verifyUser } from './controllers/accessControllers';

// database

const { PG_HOST, PG_PORT, PG_DATABASE, PG_USER, PG_PASSWORD } = process.env;
const initOptions: IOptions<{}> = {};
const configuration = {
  host: PG_HOST,
  port: PG_PORT ? parseInt(PG_PORT, 10) : 0,
  database: PG_DATABASE,
  user: PG_USER,
  password: PG_PASSWORD
};
const pgp: IMain = pgPromise(initOptions);
const db = pgp(configuration) as IDatabase<{}>;

// passport

passport.use(new BasicStrategy(verifyUser(db)));

// express

var app = express();
app.use(compression());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false}));
app.use(passport.initialize());

// routes

app.get('/', function(_req, res): void {
  var message = 'running OK';
  message += '<br>PG_PORT=' + PG_PORT;
  message += '<br>NODE_ENV=' + process.env.NODE_ENV;
  res.send(message);
});

app.get('/test', getTimestamp(db));

app.get('/users',
  passport.authenticate('basic', { session: false }),
  getUsers(db));

app.get('/users/:id',
  passport.authenticate('basic', { session: false }),
  getUserById(db));

app.post('/users', addUser(db));

app.put('/users/:id',
  passport.authenticate('basic', { session: false }),
  updateUser(db));

app.delete('/users/:id',
  passport.authenticate('basic', { session: false }),
  deleteUser(db));

app.use((_req, res): void => {
  res.status(404).send('Not found - HTTP error 404');
});

export = app;
