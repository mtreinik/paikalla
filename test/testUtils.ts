import { Response } from 'express';

// used for strong typing of response.send parameter
export interface ResponseWithTypedSend<T> extends Response {
  send: (s: T) => Response;
}

export const emptyFunction = (): void => {}
