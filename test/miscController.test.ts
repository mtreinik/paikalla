import { getTimestamp } from '../controllers/miscControllers';
import { Substitute, Arg } from '@fluffy-spoon/substitute';
import { IDatabase } from 'pg-promise';
import { Request } from 'express';
import { expect } from 'chai';
import { ResponseWithTypedSend, emptyFunction } from './testUtils';

describe('miscellaneous controllers', function(): void {

  it('returns timestamp', async function (): Promise<void> {
    const db = Substitute.for<IDatabase<{}>>();
    const data = '{"now": "2019-05-10 10:55:10.782926+00"}';
    db.one(Arg.any()).returns(Promise.resolve(data));

    const request = Substitute.for<Request>();
    const response = Substitute.for<ResponseWithTypedSend<string>>();

    const requestHandler = getTimestamp(db);
    const result = await requestHandler(request, response, emptyFunction);

    expect(result).to.equal(undefined);
    db.received().any('select now()');
    response.received().send(data);
  });

});
