import { checkUserIsAuthenticated, verifyUser, VerifiedUserOrBoolean, VerifyError } from '../controllers/accessControllers';
import { Substitute, Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import { IDatabase } from 'pg-promise';
import { Request } from 'express';
import { expect } from 'chai';
import { ResponseWithTypedSend } from './testUtils';

describe('user authentication', function(): void {

  // initialize mocked entities

  let request: SubstituteOf<Request>;
  let response: SubstituteOf<ResponseWithTypedSend<string>>;

  beforeEach('initialize db, request and response', function(): void {
    request = Substitute.for<Request>();
    response = Substitute.for<ResponseWithTypedSend<string>>();
  });

  // checkUserIsAuthenticated

  it('checks user is authenticated', function(): void {
    request.user.returns({ id: 'username' });

    const result = checkUserIsAuthenticated('username', request, response);

    expect(result).to.equal(true);
    response.didNotReceive().status(Arg.any());
    response.didNotReceive().send(Arg.any());
  });

  it('checks user is not authenticated', function(): void {
    request.user.returns({ id: 'username1' });

    const result = checkUserIsAuthenticated('username2', request, response);

    expect(result).to.equal(false);
    response.received().status(401);
    response.received().send('Unauthorized.');
  });

});

describe('user verification', function(): void {

  // initialize mocked database and fields set by doneFunction

  let db: SubstituteOf<IDatabase<{}>>;
  let error: VerifyError;
  let user: VerifiedUserOrBoolean;
  let doneFunctionCalledTimes: number;

  const doneFunction = function(e: VerifyError, u?: VerifiedUserOrBoolean): void {
    doneFunctionCalledTimes++;
    error = e;
    if (u) {
      user = u;
    }
  };

  beforeEach('initialize db, request and response', function(): void {
    db = Substitute.for<IDatabase<{}>>();
    error = undefined;
    user = false;
    doneFunctionCalledTimes = 0;
  });

  // verifyUser

  it('verifies user', async function(): Promise<void> {
    const data = {
      id: 1,
      role: 'user',
      password: '$2a$12$vI3nUib3Vs422/jAZ/ues.cAlifvNdGM71xpiBT.TFteWd9xUYrDq'
    };
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    const requestHandler = verifyUser(db);
    const result = await requestHandler('foo@example.com', 'password', doneFunction);

    expect(result).to.equal(undefined);
    expect(error).to.equal(undefined);
    expect(user).to.have.property('id', 1);
    expect(user).to.have.property('role', 'user');
    expect(doneFunctionCalledTimes).to.equal(1);
  });

  it('does not verify user when user was not found', async function(): Promise<void> {
    // database returns no user
    const data = null;
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    const requestHandler = verifyUser(db);
    const result = await requestHandler('foo@example.com', 'password', doneFunction);

    expect(result).to.equal(undefined);
    expect(error).to.equal(undefined);
    expect(user).to.equal(false);
    expect(doneFunctionCalledTimes).to.equal(1);
  });

  it('does not verify user with incorrect password', async function(): Promise<void> {
    const data = {
      id: 1,
      role: 'user',
      password: '$2a$12$vI3nUib3Vs422/jAZ/ues.cAlifvNdGM71xpiBT.TFteWd9xUYrDq'
    };
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    const requestHandler = verifyUser(db);
    const result = await requestHandler('foo@example.com', 'wrongpassword', doneFunction);

    expect(result).to.equal(undefined);
    expect(error).to.equal(undefined);
    expect(user).to.equal(false);
    expect(doneFunctionCalledTimes).to.equal(1);
  });

});
