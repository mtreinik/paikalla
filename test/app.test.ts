import request from 'supertest';
import app from '../app';

describe('app testing', function (): void {
  it('has the default page', function (done): void {
    request(app)
      .get('/')
      .expect(/running OK/, done);
  });

  it('invalid path returns HTTP error 404', function (done): void {
    request(app)
      .get('/invalid_path')
      .expect(/HTTP error 404/, done);
  });
});

describe('app testing with database connection', function (): void {

  before(function(): void {
    if (process.env.PG_TESTING !== 'true') {
      this.skip();
    }
  });

  it('returns current system date from database', function (done): void {
    request(app)
      .get('/test')
      .expect(/"now"/, done);
  });

  it('again returns current system date from database', function (done): void {
    request(app)
      .get('/test')
      .expect(/"now"/, done);
  });

});
