import { getUsers, getUserById, addUser, updateUser, deleteUser } from '../controllers/userControllers';
import { Substitute, SubstituteOf, Arg } from '@fluffy-spoon/substitute';
import { IDatabase } from 'pg-promise';
import { Request } from 'express';
import { expect } from 'chai';
import { ResponseWithTypedSend, emptyFunction } from './testUtils';

describe('user controllers', function(): void {

  // initialize mocked entities

  let db: SubstituteOf<IDatabase<{}>>;
  let request: SubstituteOf<Request>;
  let responseSendingStringArray: SubstituteOf<ResponseWithTypedSend<string []>>;
  let responseSendingString: SubstituteOf<ResponseWithTypedSend<string>>;

  beforeEach(function initDbAndRequest(): void {
    db = Substitute.for<IDatabase<{}>>();
    request = Substitute.for<Request>();
    responseSendingStringArray = Substitute.for<ResponseWithTypedSend<string []>>();
    responseSendingString = Substitute.for<ResponseWithTypedSend<string>>();
  });

  // getUsers

  it('gets users', async function(): Promise<void> {
    const data = ['userdata1', 'userdata2'];
    db.any(Arg.any()).returns(Promise.resolve(data));

    const requestHandler = getUsers(db);
    const result = await requestHandler(request, responseSendingStringArray, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingStringArray.received().send(data);
  });

  // getUserById

  it('gets user by id', async function(): Promise<void> {
    const data = 'userdata2';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));
    request.params.returns({ id: '123' });
    request.user.returns({ id: 123 });

    const requestHandler = getUserById(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().send(data);
  })

  it('returns error if user is not found', async function(): Promise<void> {
    // database query for user returns null (= user not found)
    const data = null;
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));
    request.params.returns({ id: '123' });
    request.user.returns({ id: 123 });

    const requestHandler = getUserById(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(404);
    responseSendingString.received().send('No user found with id 123.');
  })

  it('refuses getting user other than that is authenticated', async function(): Promise<void> {
    const data = 'userdata3';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));
    // user ids intentionally different:
    request.params.returns({ id: '123' });
    request.user.returns({ id: 456 });

    const requestHandler = getUserById(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(401);
    responseSendingString.received().send('Unauthorized.');
  })

  // addUser

  it('adds user', async function(): Promise<void> {
    const data = 'userdata4';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      organizationId: '1',
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      role: 'user',
      password: 'password123'
    });

    const requestHandler = addUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().send(data);
  });

  it('returns error if adding user fails', async function(): Promise<void> {
    // database query does not return id of inserted user (= insert failed)
    const data = null;
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      organizationId: '1',
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      role: 'user',
      password: 'password123'
    });

    const requestHandler = addUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(500);
    responseSendingString.received().send('Unable to save user.');
  });

  // updateUser

  it('updates user', async function(): Promise<void> {
    const data = 'userdata4';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    request.params.returns({ id: '123' })
    request.user.returns({ id: 123 });

    const requestHandler = updateUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().send(data);
  });

  it('returns error if updating user fails', async function(): Promise<void> {
    // database query returns null (= update failed)
    const data = null;
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    request.params.returns({ id: '123' })
    request.user.returns({ id: 123 });

    const requestHandler = updateUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(500);
    responseSendingString.received().send('Unable to update user.');
  });

  it('refuses updating user other than that authenticated', async function(): Promise<void> {
    const data = 'userdata5';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    // user ids intentionally different:
    request.params.returns({ id: '123' })
    request.user.returns({ id: 456 });

    const requestHandler = updateUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(401);
    responseSendingString.received().send('Unauthorized.');
  });

  // deleteUser

  it('deletes user', async function(): Promise<void> {
    const data = 'userdata6';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    request.params.returns({ id: '123' })
    request.user.returns({ id: 123 });

    const requestHandler = deleteUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().send(data);
  });

  it('returns error if deleting user fails', async function(): Promise<void> {
    const data = null;
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    request.params.returns({ id: '123' })
    request.user.returns({ id: 123 });

    const requestHandler = deleteUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(500);
    responseSendingString.received().send('Unable to delete user.');
  });

  it('refuses to delete user other than that authenticated', async function(): Promise<void> {
    const data = 'userdata7';
    db.oneOrNone(Arg.any(), Arg.any()).returns(Promise.resolve(data));

    request.body.returns({
      email: 'no@example.com',
      firstName: 'First',
      lastName: 'Last',
      nickname: 'nick',
      password: 'password123'
    });
    // user ids intentionally different:
    request.params.returns({ id: '123' })
    request.user.returns({ id: 456 });

    const requestHandler = deleteUser(db);
    const result = await requestHandler(request, responseSendingString, emptyFunction);

    expect(result).to.equal(undefined);
    responseSendingString.received().status(401);
    responseSendingString.received().send('Unauthorized.');
  });

});
